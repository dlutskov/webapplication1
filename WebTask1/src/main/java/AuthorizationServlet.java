

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import web.processing.*;




/**
 * Servlet implementation class AuthorizationServlet
 */
public class AuthorizationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Login button processing
		if (request.getParameter("loginButton") != null) {
			String username = request.getParameter("username");
	        String password = request.getParameter("password");
	        ValidationHandler validator = new ValidationHandler();
	        if (validator.isAuthorizationValid(username, password)) {
	        	HttpSession session = request.getSession();
	            DataContainer storage = new DataContainer();
	            PersonalData data = storage.getUserData(username);
	        	session.setAttribute("personalData", data);
	        	session.setAttribute("currentUser", "true");
	            response.sendRedirect("personalPage.jsp");
	        } else {
	        	RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
	            PrintWriter out= response.getWriter();
	            request.setAttribute("username", username);
	            out.println("<font color=red><b>" + validator.getMessage() + "</b></font>");
	            rd.include(request, response);
	        }
		}
		
		//
		// Registration page
		else if ((request.getParameter("registrationButton") != null)) {
			response.sendRedirect("registration.jsp");
		}
	}

}
