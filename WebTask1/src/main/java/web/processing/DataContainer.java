package web.processing;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import org.apache.tomcat.jni.Directory;

/**
 * Allow to store personal data. Data of every user will be stored in xml file
 */
public class DataContainer {

	/**
	 * Name of folder which will contain files
	 */
	private static final String folder = "users";
	
	
	/**
	 * Information about errors which were emerged
	 */
	private String errorMessage = "";
	
	// METHODS
	public DataContainer() {		
		File dir = new File(folder);
		if (!dir.exists()) {
			dir.mkdir();
		}
	}
	
	/**
	 * Return error message
	 */
	public String getMessage()
	{
		return errorMessage;
	}
	
	/**
	 * Return true if file which contains user data exists
	 */
	public boolean isUserExist(String username) {
		String path = folder + "/" + username.hashCode() + username + ".xml";
		return new File(path).exists();
	}
	
	/**
	 * Create new xml file and write user data there
	 */
	public boolean addNewUser(PersonalData data) {
		JAXBContext context;
		try {
			String name = data.getUsername();
			String filename = folder + "/" + name.hashCode() + name + ".xml";
			context = JAXBContext.newInstance(PersonalData.class);
			Marshaller m = context.createMarshaller();
			m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			// Write to System.out
		    m.marshal(data, System.out);
			// Write to File
			m.marshal(data, new File(filename));
			return true;
		} catch (JAXBException e) {
			errorMessage = e.getMessage();
			return false;
		}
	}
	
	/**
	 * Reading user data from particular xml and return it
	 */
	public PersonalData getUserData(String username) {
		try {
			String filename = folder + "/" + username.hashCode() + username + ".xml";
			JAXBContext context = JAXBContext.newInstance(PersonalData.class);
			Unmarshaller um = context.createUnmarshaller();;
			PersonalData data = (PersonalData) um.unmarshal(new FileReader(filename));			
			return data;
		} catch (JAXBException e) {
			errorMessage = e.getMessage();
			return null;
		} catch (FileNotFoundException e) {
			errorMessage = e.getMessage();
			return null;
		}
	}
	
	
}
