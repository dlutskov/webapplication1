package web.processing;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Allow to validate input data
 */
public class ValidationHandler {

	/**
	 * Information about errors which were emerged
	 */
	private String errorMessage;

	/**
	 * Pattern for email validation
	 */
	private static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
			+ "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

	// METHODS
	/**
	 * Return error message
	 */
	public String getMessage() {
		return errorMessage;
	}

	/**
	 * Return true if such user exists and password is fit for username
	 */
	public boolean isAuthorizationValid(String username, String password) {
		if (!isUserExist(username)) {
			errorMessage = "Such user does not exist!";
			return false;
		} else if (!isPasswordValid(username, password)) {
			errorMessage = "Wrong password! Try again.";
			return false;
		} else
			return true;
	}

	/**
	 * Return true when every text area is filled correctly and such user
	 * doesn't already exist. Thats registration is valid
	 */
	public boolean isRegistrationValid(PersonalData data) {
		if (isUpdatingValid(data)) {
			if (isUserExist(data.getUsername())) {
				errorMessage = "Such username is already existed!";
				return false;
			} else return true;
		} else
			return false;
	}
	
	/**
	 * Return true when every areas filled correctly
	 */
	public boolean isUpdatingValid(PersonalData data) {
		for (String field : data.getDataList()) {
			if (field.isEmpty()) {
				errorMessage = "Complete all text fields!";
				return false;
			}
		}
		if (!isLoginValid(data.getUsername())) {
			errorMessage = "Login shouldn't contain spaces and cant be less than 6 symbols";
			return false;
		} else if (!isEmailValid(data.getEmail())) {
			errorMessage = "Wrong email format!";
			return false;
		}
		return true;
	}
	
	/**
	 * Return true if such username is already existed
	 */
	private boolean isUserExist(String username) {
		return new DataContainer().isUserExist(username);
	}

	/**
	 * Return password of current user
	 */
	private boolean isPasswordValid(String username, String password) {
		PersonalData data = new DataContainer().getUserData(username);
		String realPassword = data.getPassword();
		return password.equals(realPassword);
	}
	
	/**
	 * Return true if username doesn't contain spaces and more than 6 symbols
	 */
	private boolean isLoginValid(String username) {
		if ((username.contains(" ")) || (username.length() < 6)) 
			return false;
		else 
			return true;
			
	}
	
	/**
	 * Return true when email is valid
	 */
	private boolean isEmailValid(String email) {
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}
	

}
