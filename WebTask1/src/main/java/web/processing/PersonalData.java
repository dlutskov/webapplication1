package web.processing;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Contain variables of user information, and methods which allow get and set values
 */
@XmlRootElement
public class PersonalData {
	@XmlElement
	private String username = "";
	
	private String password = "";

	private String firstName = "";

	private String lastName = "";

	private String email = "";

	private String phone = "";

	// CONSTRUCTOR
	public PersonalData() {
	}

	public PersonalData(String username, String password, String firstName,
			String lastName, String email, String phone) {
		this.username = username;
		this.password = password;
		this.email = email;
		this.phone = phone;
		this.firstName = firstName;
		this.lastName = lastName;
	}

	// GET & SET METHODS
	public String getUsername() {
		return username;
	}
	
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Get all personal data in a List
	 */
	public List<String> getDataList() {
		List<String> list = new ArrayList<String>();
		list.add(username);
		list.add(password);
		list.add(firstName);
		list.add(lastName);
		list.add(email);
		list.add(phone);
		return list;
	}

}

