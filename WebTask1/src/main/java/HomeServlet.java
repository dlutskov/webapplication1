

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import web.processing.DataContainer;
import web.processing.PersonalData;
import web.processing.ValidationHandler;

/**
 * Servlet implementation class HomeServlet
 */
public class HomeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Save data button
		if ((request.getParameter("saveButton")) != null) {
			PersonalData data = getUserData(request, response);
			ValidationHandler validator = new ValidationHandler();
			HttpSession session = request.getSession(true);
            PrintWriter out= response.getWriter();			
            if (validator.isUpdatingValid(data)) {					            
            	new DataContainer().addNewUser(data);
            	session.setAttribute("personalData", data);
	            String msg = "Changes were successfully saved!";
	            out.println("<font color=blue><b>" + msg + "</b></font>");             	
			}
			else {
	            out.println("<font color=red><b>" + validator.getMessage() + "</b></font>"); 
			}
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/personalPage.jsp");
			rd.include(request, response); 
		}
		// Logout button
		else if ((request.getParameter("logoutButton") != null)) {
			HttpSession session = request.getSession(false);
	        if(session != null){
	        	session.setAttribute("currentUser","false");
	            session.invalidate();	            
	        }
	        response.sendRedirect("index.jsp");
		}
	}
	
	/**
	 * Getting information from jsp page
	 */
	private PersonalData getUserData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		PersonalData personalData = new PersonalData(username, password,
				firstName, lastName, email, phone);
		
		return personalData;
	}

}
