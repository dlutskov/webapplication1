import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * Servlet Filter implementation class SecurityFilter
 */
public class SecurityFilter implements Filter {

	public void init(FilterConfig filterConfig) throws ServletException {
	}

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {

		HttpServletResponse resp = (HttpServletResponse) response;
		HttpServletRequest req = (HttpServletRequest) request;
		String servletPath = req.getServletPath();

		if (servletPath.equals("/personalPage.jsp")) {
			HttpSession session = req.getSession(false);
			String currentUser = (String)session.getAttribute("currentUser");
			if ((session != null) &&(currentUser == "true")) {
				chain.doFilter(req, resp);
				return;
			}
			System.out.println("REDIRECT");
			resp.sendRedirect("index.jsp");
		} else
			chain.doFilter(req, resp);
	}

}
