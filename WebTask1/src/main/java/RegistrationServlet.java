

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import web.processing.DataContainer;
import web.processing.PersonalData;
import web.processing.ValidationHandler;

/**
 * Servlet implementation class RegistrationServlet
 */
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		PersonalData data = getUserData(request, response);
		ValidationHandler validator = new ValidationHandler();		
        PrintWriter out= response.getWriter();			
        // Forward to sign in page
        if (validator.isRegistrationValid(data)) {					            
        	new DataContainer().addNewUser(data);
            String msg = "Registration is complete. You can sign in!";
            out.println("<font color=blue><b>" + msg + "</b></font>"); 
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/index.jsp");
    		rd.include(request, response); 
		}
        // Forward back to registration page
		else {
            out.println("<font color=red><b>" + validator.getMessage() + "</b></font>");
            RequestDispatcher rd = getServletContext().getRequestDispatcher("/registration.jsp");
            request.setAttribute("username", data.getUsername());
            request.setAttribute("password", data.getPassword());
            request.setAttribute("firstName", data.getFirstName());
            request.setAttribute("lastName", data.getLastName());
            request.setAttribute("email", data.getEmail());
            request.setAttribute("phone", data.getPhone());
    		rd.include(request, response);
		}
         
	}
	
	/**
	 * Getting information from jsp page
	 */
	private PersonalData getUserData(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String email = request.getParameter("email");
		String phone = request.getParameter("phone");
		String username = request.getParameter("username");
		String password = request.getParameter("password");

		PersonalData personalData = new PersonalData(username, password,
				firstName, lastName, email, phone);
		
		return personalData;
	}

}
