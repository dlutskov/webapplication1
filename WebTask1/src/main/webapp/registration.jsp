<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="styles.css" />
	<title>Sign up!</title>
</head>
<body>

	<table>
		<form method="post" action="registration">
			<tr>
				<td><b>First Name:</b>
				</td>
				<td><input name="firstName" id="input" maxlength="30"
					value="${requestScope.firstName}" >
				</td>
			</tr>
			<tr>
				<td><b>Last Name:</b>
				</td>
				<td><input name="lastName" id="input" maxlength="30"
					value="${requestScope.lastName}" >
				</td>
			</tr>
			<tr>
				<td><b>E-Mail:</b>
				</td>
				<td><input name="email" id="input" maxlength="30"
					value="${requestScope.email}" >
				</td>
			</tr>
			<tr>
				<td><b>Phone:</b>
				</td>
				<td><input name="phone" id="input" value="${requestScope.phone}"
				onkeyup="this.value=this.value.replace(/[^\d]/,'')"maxlength="12">
				</td>
			</tr>
			<tr>
				<td><br>
				</td>
			</tr>
			<tr>
				<td><b>Username:</b>
				</td>
				<td><input name="username" id="input" maxlength="30"
					value="${requestScope.username}"  >
				</td>
			</tr>
			<tr>
				<td><b>Password:</b>
				</td>
				<td><input type="password" name="password" id="input"
					maxlength="30" value="${requestScope.password}" >
				</td>
			</tr>
			<tr>
				<td><br>
				</td>
			</tr>
			<tr>
				<td></td>
				<td>
				<input type="submit" id="button" value="Registrate">
				</td>
			</tr>
		</form>
	</table>

</body>
</html>