<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
	<link rel="stylesheet" type="text/css" href="styles.css" />
	<title>Welcome!</title>
</head>
<body>
	<table>
		<form method="post" action="authorization">
			<tr>
				<td><b>Username:</b>
				</td>
				<td><input name="username" id="input" maxlength="30" value="${param.username}" >
				</td>
			</tr>
			<tr>
				<td><b>Password:</b>
				</td>
				<td><input type="password" name="password" id="input"
					maxlength="30" >
				</td>
			</tr>
			<tr>
				<td></td>
				<td><input type="submit" name="loginButton" id="button"
					value="Login"> <input type="submit"
					name="registrationButton" id="button" value="Registration">
				</td>
			</tr>
		</form>
	</table>
</body>
</html>